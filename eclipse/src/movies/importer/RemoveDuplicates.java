//Vashti Lanz Rubio
//1931765
package movies.importer;
import java.util.ArrayList;

public class RemoveDuplicates extends Processor{
	public RemoveDuplicates(String source, String destination) {
		super(source,destination,false);
	}
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> noDuplicate = new ArrayList<String>();
		for(int i = 0; i < input.size(); i++) {
			if(!noDuplicate.contains(input.get(i))) {
					noDuplicate.add(input.get(i));
				}
			}
		return noDuplicate;
	}
}
