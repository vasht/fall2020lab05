//Vashti Lanz Rubio
//1931765
package movies.importer;
import java.util.ArrayList;

public class LowercaseProcessor extends Processor{
	public LowercaseProcessor(String source, String destination) {
		super(source,destination,true);
	}
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> asLower = new ArrayList<String>();
		for(int i = 0; i < input.size(); i++) {
			asLower.add(input.get(i).toLowerCase());
		}
		return asLower;
	}
}
