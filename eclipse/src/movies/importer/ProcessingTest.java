//Vashti Lanz Rubio
//1931765
package movies.importer;
import java.io.IOException;

public class ProcessingTest{
	public static void main(String[] args) throws IOException{
		String source = "D:\\Courses\\java310\\fall2020lab05\\texts";
		String destination = "D:\\Courses\\java310\\fall2020lab05\\texts_destination";
		LowercaseProcessor lp = new LowercaseProcessor(source,destination);
		RemoveDuplicates rd = new RemoveDuplicates(destination,destination);
		lp.execute();
		rd.execute();
	}
}
